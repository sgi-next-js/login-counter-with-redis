import Redis from 'ioredis'

const redis = new Redis({
  host: '103.186.1.18',
  port: 6379,
  password: 'secret_redis'
});

export async function getUserCount(){
    const userIdKeys = await redis.keys('userid_*');
    console.log("User Count:", userIdKeys.length);
    return userIdKeys.length;
}