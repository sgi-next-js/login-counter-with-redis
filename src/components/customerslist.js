import {useLiveQuery} from "dexie-react-hooks";
import {customerTable} from "../database.config";
import {useState} from "react";

const CustomersList = () => {
    const [lower, setLower] = useState(0);
    const [upper, setUpper] = useState(10000);
    const customer = useLiveQuery(() => customerTable.where("dept")
            .between(lower, upper).toArray(),
        [lower, upper]
    );

    return <div>
        <h1>Customers</h1>
        lower: <input type="number" value={lower} onChange={(e) => setLower(Number(e.target.value))}/> & upper: <input type="number" value={upper} onChange={(e) => setUpper(Number(e.target.value))}/>
        <ul>
        {customer?.map(customer => <li key={customer.id}>
            {customer.name}, {customer.dept}$
        </li>)}
    </ul></div>
}

export default CustomersList;