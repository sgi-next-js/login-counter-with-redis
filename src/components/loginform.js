import Link from 'next/link';

function LoginForm() {
  return (
      <form className="login-form" action="/api/auth/login" method="post">
        <div className="mb-3">
          <input name="email" type="text" className="form-control" placeholder="Email address or phone number"
                required />
        </div>
        <div className="mb-3">
          <input name="password" type="password" className="form-control" placeholder="Password"
                required />
        </div>
        <div className="text-center pt-3 pb-3">
          <Link href="#">Lupa password?</Link>
          <hr/>
          <button type="submit" className="btn btn-primary btn-lg mt-3">Login</button>
        </div>
      </form>
    )
}

export default LoginForm;
