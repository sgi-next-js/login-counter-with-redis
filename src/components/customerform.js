import {customerTable} from "../database.config";

export default function CustomerForm () {
  const createCustomer = async (event) => {
      event.preventDefault();
      const customer = {
          name: event.target.name.value,
          dept: Number(event.target.dept.value)
          };
      try {
          // Add the new customer!
          const id = await customerTable.add(customer);
          console.info(`A new customer was created with id ${id}`);
          event.target.reset()

      } catch (error) {
          console.error(`Failed to add ${customer}: ${error}`);
      }
    }
  return ( 
      <>
          <h1>Create customer</h1>
          <form onSubmit={createCustomer}>
              <label htmlFor="name">Name:</label><br />
              <input type="text" id="name" name="name"/><br /><br />
              <label htmlFor="dept">Dept:</label><br />
              <input type="number" id="dept" name="dept"/><br /><br />
              <button type="submit">Create</button>
          </form>
      </>
  )
}