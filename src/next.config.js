module.exports = {
    serverRuntimeConfig: {
      // Will only be available on the server side
      redisCredential: {
            host: '103.186.1.18', 
            port: 6379, 
            password: 'secret_redis'
        },
      userAccessLimit: 3,  
      secondSecret: process.env.SECOND_SECRET, // Pass through env variables
    },
    publicRuntimeConfig: {
      // Will be available on both server and client
      staticFolder: '/static',
    },
  }