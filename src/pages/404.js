import Link from 'next/link'

export default function PageNotFound() {
  return <>
    <div className="d-flex align-items-center justify-content-center vh-100">
    <div class="text-center">
    <h1 class="display-1 fw-bold">404</h1>
    <p className="fs-3"> 
      <span className="text-danger">Opps!</span>{" "}
      Halaman tidak ditemukan.
    </p>
    <p className="lead">
      Pastikan anda mengakses tautan yang tepat.
    </p>
    <Link href="/" className="btn btn-primary">Go Home</Link>
    </div>
    </div>
  </>
}