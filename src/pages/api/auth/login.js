import Redis from 'ioredis'

const redis = new Redis({
  host: '103.186.1.18',
  port: 6379,
  password: 'secret_redis'
});

export default async function login(req, res) {
  const userIdKeys = await redis.keys('userid_*');
  console.log(userIdKeys);

  if( userIdKeys.length > 3 ){
    return res.redirect(503, '/503');
  } else {
    const body = req.body
    console.log('body: ', body)
    
    if (body.password=='admin') {
      redis.set('userid_'+body.email, true, "EX", 60);
      return res.redirect(307, '/home');
    } else {
      return res.redirect(307, '/login?loginfailed=true');
    }
  }
}
  