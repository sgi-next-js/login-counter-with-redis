import Link from 'next/link'

export default function ServerIsBusy() {
  return <>
    <div className="d-flex align-items-center justify-content-center vh-100">
    <div class="text-center">
    <h1 class="display-1 fw-bold">503</h1>
    <p className="fs-3"> 
      <span className="text-danger">Opps!</span>{" "}
      Halaman tidak tersedia karena server saat ini sedang sibuk.
    </p>
    <p className="lead">
      Silahkan coba akses kembali beberapa saat lagi.
    </p>
    <Link href="/" className="btn btn-primary">Go Home</Link>
    </div>
    </div>
  </>
}