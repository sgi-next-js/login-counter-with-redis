import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import LoginForm from '@/components/loginform';
import {getUserCount} from '../lib/authlib';


export default function Home({isOverlimit}) {
  return (
<Container>
  { isOverlimit &&
    <div id="loginfailed" className="alert alert-danger alert-dismissable">
        <strong>Server sibuk!</strong> Silahkan coba lagi
    </div>
  }
  <Row className="align-items-center justify-content-center vh-100">
    <Col className="col-md-7">
          <img src="https://www.prasetiyamulya.ac.id/wp-content/uploads/2020/01/Logo-Universitas-Prasetiya-Mulya.png" />
          <h3>Aplikasi Rencana Studi</h3>
    </Col>
    <Col className="col-md-5">
      <LoginForm/>
    </Col>
  </Row>
</Container>
  )
}

export async function getServerSideProps() {
  // Fetch data from external API
  const userCount = await getUserCount();
  console.log("getServerSideProps", userCount);

  if(userCount>2){
    return { props: { isOverlimit: true  } }
    //503
  }
  return { props: { userCount } }
}