import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import LoginForm from '@/components/loginform';
import { useRouter } from 'next/router'
import { useState, useEffect } from 'react';

export default function Login() {
  const [data, setData] = useState('')

  useEffect(() => {
    const urlSearchParams = new URLSearchParams(window.location.search)
    const data = urlSearchParams.get('loginfailed')
    setData(data)
    /*if(data=="true"){
      const { Alert } = require("bootstrap");
      const myAlert = new Alert('#loginfailed');
      myAlert.show();
    }
    console.log('DATA')
    console.log(data)*/
  }, [])


  /*const showModal = () => {
      const { Modal } = require("bootstrap");
      const myModal = new Modal("#exampleModal");
      console.log('Button clicked')
      myModal.show();
    };*/

  return (
<Container>
  {data=='true' && (
    <div id="loginfailed" className="alert alert-danger alert-dismissable">
      <strong>Login gagal!</strong> Email dan/atau password anda salah
    </div>
    )}
  
  <Row className="align-items-center justify-content-center vh-100">
    <Col className="col-md-5">
      <h3>Aplikasi Rencana Studi</h3>
      <LoginForm/>
    </Col>
  </Row>
</Container>
  )
}
